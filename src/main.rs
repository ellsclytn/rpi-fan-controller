extern crate systemstat;

use rppal::gpio::{Gpio, OutputPin};
use std::error::Error;
use std::thread;
use std::time::Duration;
use systemstat::{Platform, System};

// BCM Pin
const GPIO_FAN: u8 = 21;

fn main() -> Result<(), Box<dyn Error>> {
    let sys = System::new();
    let mut pin = Gpio::new()?.get(GPIO_FAN)?.into_output();

    loop {
        match sys.cpu_temp() {
            Ok(cpu_temp) => control_fan(&mut pin, cpu_temp),
            Err(x) => println!("\nCPU temp: {}", x),
        }
        thread::sleep(Duration::from_millis(5000));
    }
}

fn control_fan(fan: &mut OutputPin, temp: f32) {
    if temp >= 70.0 {
        fan.set_high();
    } else {
        fan.set_low();
    }
}
